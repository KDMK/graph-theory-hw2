package hr.fer.zpm.tg;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class GraphColoring {

    private static final int NUM_OF_COLORS = 4;

    /* A utility function to check if the current
      color assignment is safe for vertex v */
    private static boolean isSafe(int v, int[][] graph, int[] color,
                           int c, int numOfVertex) {
        for (int i = 0; i < numOfVertex; i++)
            if (graph[v][i] == 1 && c == color[i])
                return false;
        return true;
    }

    /* A recursive utility function to solve m
       coloring  problem */
    private static boolean graphColoringUtil(int[][] graph, int numOfVertex,
                                      int[] color, int currentVertex) {
        /* base case: If all vertices are assigned
           a color then return true */
        if (currentVertex == numOfVertex)
            return true;

        /* Consider this vertex v and try different
           colors */
        for (int c = 1; c <= NUM_OF_COLORS; c++) {
            /* Check if assignment of color c to v
               is fine*/
            if (isSafe(currentVertex, graph, color, c, numOfVertex)) {
                color[currentVertex] = c;

                /* recur to assign colors to rest
                   of the vertices */
                if (graphColoringUtil(graph, numOfVertex,
                        color, currentVertex + 1))
                    return true;

                /* If assigning color c doesn't lead
                   to a solution then remove it */
                color[currentVertex] = 0;
            }
        }

        /* If no color can be assigned to this vertex
           then return false */
        return false;
    }

    /* This function solves the m Coloring problem using
       Backtracking. It mainly uses graphColoringUtil()
       to solve the problem. It returns false if the m
       colors cannot be assigned, otherwise return true
       and  prints assignments of colors to all vertices.
       Please note that there  may be more than one
       solutions, this function prints one of the
       feasible solutions.*/
    private static boolean graphColoring(int[][] graph, int numberOfVertex) {
        // Initialize all color values as 0. This
        // initialization is needed correct functioning
        // of isSafe()
        int[] color = new int[numberOfVertex];
        for (int i = 0; i < numberOfVertex; i++)
            color[i] = 0;

        // Call graphColoringUtil() for vertex 0
        if (!graphColoringUtil(graph, numberOfVertex, color, 0)) {
            System.out.println("Solution does not exist");
            return false;
        }

        // Print the solution
        printSolution(color, numberOfVertex);
        return true;
    }

    /* A utility function to print solution */
    private static void printSolution(int[] color, int numberOfVertex) {
        System.out.println("Solution Exists: Following" +
                " are the assigned colors");
        for (int i = 0; i < numberOfVertex; i++)
            System.out.print(" " + color[i] + " ");
        System.out.println();
    }

    public static void main(String[] args) throws IOException {
        // Napišite program koji za zadani jednostavni graf G ispituje je li vršno 4-obojiv
        // (χ(G)≤4) te, ako jest, pronalazi jedno vršno 4-bojanje od G.

        if(args.length != 1) {
            System.out.println("Usage: java GraphColoring /path/to/input/file");
            System.exit(1);
        }

        List<String> lines = Files.readAllLines(Paths.get(args[0])).stream().filter(l -> l.trim().length() != 0).collect(Collectors.toList());


        int dimension = Integer.parseInt(lines.get(0));
        lines.remove(0);

        int[][] matrix = new int[4][4];
        int row = 0;
        for(String line: lines) {
            int col = 0;
            for(String adj: line.split(",")) {
                matrix[row][col++] = Integer.parseInt(adj.trim());
            }
            row++;
        }

        graphColoring(matrix, dimension);
    }
}
